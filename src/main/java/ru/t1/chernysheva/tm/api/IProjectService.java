package ru.t1.chernysheva.tm.api;

import ru.t1.chernysheva.tm.model.Project;

public interface IProjectService extends IProjectRepository {

    Project create(String name, String description);

}
