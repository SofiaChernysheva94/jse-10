package ru.t1.chernysheva.tm.api;

import ru.t1.chernysheva.tm.model.Task;

public interface ITaskService extends ITaskRepository {

    Task create(String name, String description);

}
