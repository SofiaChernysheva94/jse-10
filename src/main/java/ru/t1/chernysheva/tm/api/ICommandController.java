package ru.t1.chernysheva.tm.api;

public interface ICommandController {
    void showErrorArgument();

    void showSystemInfo();

    void showErrorCommand();

    void showVersion();

    void showAbout();

    void showHelp();

}
