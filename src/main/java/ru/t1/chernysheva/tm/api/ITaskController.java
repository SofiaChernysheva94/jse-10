package ru.t1.chernysheva.tm.api;

public interface ITaskController {

    void showTasks();

    void createTask();

    void clearTasks();

}
