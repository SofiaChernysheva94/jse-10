package ru.t1.chernysheva.tm.service;

import ru.t1.chernysheva.tm.model.Command;
import ru.t1.chernysheva.tm.api.ICommandRepository;
import ru.t1.chernysheva.tm.api.ICommandService;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getCommands() {
        return commandRepository.getCommands();
    }

}
