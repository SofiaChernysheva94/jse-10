package ru.t1.chernysheva.tm.repository;

import ru.t1.chernysheva.tm.api.ICommandRepository;
import ru.t1.chernysheva.tm.constant.ArgumentsConst;
import ru.t1.chernysheva.tm.constant.CommandConst;
import ru.t1.chernysheva.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private static final Command HELP = new Command(CommandConst.HELP, ArgumentsConst.HELP, "Show command list.");

    private static final Command VERSION = new Command(CommandConst.VERSION, ArgumentsConst.VERSION, "Show version info.");

    private static final Command ABOUT = new Command(CommandConst.ABOUT, ArgumentsConst.ABOUT, "Show developer info.");

    private static final Command INFO = new Command(CommandConst.INFO, ArgumentsConst.INFO, "Show system info.");

    private static final Command EXIT = new Command(CommandConst.EXIT, null, "Close application.");

    private static final Command PROJECT_LIST = new Command(CommandConst.PROJECT_LIST, null, "Show project list.");

    private static final Command PROJECT_CREATE = new Command(CommandConst.PROJECT_CREATE, null, "Create new project.");

    private static final Command PROJECT_CLEAR = new Command(CommandConst.PROJECT_CLEAR, null, "Delete all projects.");

    private static final Command TASK_LIST = new Command(CommandConst.TASK_LIST, null, "Show task list.");

    private static final Command TASK_CREATE = new Command(CommandConst.TASK_CREATE, null, "Create new task.");

    private static final Command TASK_CLEAR = new Command(CommandConst.TASK_CLEAR, null, "Delete all tasks.");

    private static final Command[] COMMANDS = new Command[] {
            HELP, VERSION, ABOUT, INFO,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            EXIT
    };

    public Command[] getCommands() {
        return COMMANDS;
    }

}
